#!/bin/bash

PWD="$( cd "$( dirname "$0" )" && pwd )"

ln -nfs $PWD/.bin $HOME/.bin
ln -nfs $PWD/.gemrc $HOME/.gemrc
ln -nfs $PWD/.gitconfig $HOME/.gitconfig
ln -nfs $PWD/.tmux.conf $HOME/.tmux.conf
ln -nfs $PWD/.tmuxinator $HOME/.tmuxinator
ln -nfs $PWD/.zlogin $HOME/.zlogin
ln -nfs $PWD/.zlogout $HOME/.zlogout
ln -nfs $PWD/.zprezto $HOME/.zprezto
ln -nfs $PWD/.zpreztorc $HOME/.zpreztorc
ln -nfs $PWD/.zprofile $HOME/.zprofile
ln -nfs $PWD/.zshenv $HOME/.zshenv
ln -nfs $PWD/.zshrc $HOME/.zshrc

cd $HOME/dotfiles
git submodule init
git submodule update

cd $HOME/.zprezto
git submodule init
git submodule update

mkdir -p $HOME/spring_tmp
