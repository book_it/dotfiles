#
# Executes commands at the start of an interactive session.
#
# Authors:
#   Sorin Ionescu <sorin.ionescu@gmail.com>
#

# Source Prezto.
if [[ -s "${ZDOTDIR:-$HOME}/.zprezto/init.zsh" ]]; then
  source "${ZDOTDIR:-$HOME}/.zprezto/init.zsh"
fi

# Customize to your needs...

export PATH="$HOME/.rbenv/bin:$PATH"
export SPRING_TMP_PATH="$HOME/spring_tmp"
eval "$(rbenv init -)"

source $HOME/.bin/tmuxinator.zsh

# Aliases
alias history="history 1 -1"
alias h="history | grep "

# Restarts puma for the book_it app
function resserv() {
    kill -9 $(lsof -i tcp:3000 -t)
    cd /usr/share/nginx/www/book_it
    BUNDLE_GEMFILE=/usr/share/nginx/www/book_it/Gemfile rails server -d
}
